"""Test functions used for formating results into vcf format."""

import pytest
from me_finder.tools.blast import _blast_aln_to_vcf_snp, _parse_alignment

SUBSTITUTIONS = [["atcga", "attga", {3: {"ref": "C", "alt": "T"}}]]
INSERTIONS = [
    ["atc-ga", "atctga", {3: {"ref": "C", "alt": "CT"}}],
    ["atc---ga", "atctagga", {3: {"ref": "C", "alt": "CTAG"}}],
]
DELETIONS = [
    ["atcga", "at-ga", {2: {"ref": "TC", "alt": "T"}}],
    ["atcga", "at--a", {2: {"ref": "TCG", "alt": "T"}}],
]


@pytest.mark.parametrize(
    "ref_aln,alt_aln,expected", [*SUBSTITUTIONS, *INSERTIONS, *DELETIONS]
)
def test_vcf_snp_from_aln(ref_aln, alt_aln, expected):
    """Test parsing of mutations from blast alignment.

    Test cases are derived from vcf v4.2 format specification.
    https://samtools.github.io/hts-specs/VCFv4.2.pdf
    """
    assert _blast_aln_to_vcf_snp(ref_aln, alt_aln) == expected


def test_snp_from_aln():
    """Test old code generating SNP annotation from alignment."""
    # substitution
    ref = "atcga"
    alt = "attga"
    exp = {3: {"ref": "c", "pos": 3, "alt": "t"}}
    assert _parse_alignment(alt, ref) == exp

    ref = "atcga"
    alt = "at-ga"
    exp = {3: {"ref": "c", "pos": 3, "alt": "-"}}
    assert _parse_alignment(alt, ref) == exp


def test_complex_aln_to_vcf():
    """Test complex blast alignement stings."""
    # Test insertions
    ref = "atcga"
    alt = "at-ga"
    exp = {2: {"ref": "TC", "alt": "T"}}
    assert _blast_aln_to_vcf_snp(ref, alt) == exp

    ref = "TTGAGAAAGCT"
    alt = "GT-A-AAAGCT"
    exp = {
        1: {"ref": "T", "alt": "G"},
        2: {"ref": "TG", "alt": "T"},
        4: {"ref": "AG", "alt": "A"},
    }
    assert _blast_aln_to_vcf_snp(ref, alt) == exp

    # Test complex case with multiple insertions
    ref = "C---A--GT-A-AAAGCT"
    alt = "CTGTATTTTGAGAAAGCT"
    exp = {
        1: {"ref": "C", "alt": "CTGT"},
        2: {"ref": "A", "alt": "ATT"},
        3: {"ref": "G", "alt": "T"},
        4: {"ref": "T", "alt": "TG"},
        5: {"ref": "A", "alt": "AG"},
    }
    assert _blast_aln_to_vcf_snp(ref, alt) == exp
