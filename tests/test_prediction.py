"""Test merging of hsps into one mge"""
from pathlib import Path
from unittest.mock import MagicMock, Mock

import attr
import pytest
from me_finder.errors import CoordOutOfBoundsError
from me_finder.io import ContigSequences
from me_finder.predictor.detect import _bin_hits, _detect_mges, _pad_mge_coord
from me_finder.predictor.parse import (
    _calc_tot_aln_cov,
    _get_possible_elem_coord,
    _group_hsps,
    _is_contained,
    _join_cigar,
    _parse_raw_mge_alignments,
)
from me_finder.predictor.types import ElementHit
from me_finder.tools import BlastHsp


def mock_hsp(**kwargs):
    """Mock hsp object."""
    hsp = Mock(spec=BlastHsp)
    for k, v in kwargs.items():
        setattr(hsp, k, v)
    # mock return value function
    hsp.__len__ = Mock(return_value=hsp.query_end - hsp.query_start + 1)
    return hsp


def mock_hit(**kwargs):
    """Mock hit object."""
    hit = Mock(spec=ElementHit)
    for k, v in kwargs.items():
        if k == "hsps":
            v = [mock_hsp(**h) for h in v]
        setattr(hit, k, v)
    return hit


two_hsps_same_mge = {
    "hsps": [
        {
            "query_start": 200,
            "query_end": 900,
            "subject_start": 100,
            "subject_end": 800,
        },
        {
            "query_start": 1100,
            "query_end": 1300,
            "subject_start": 1000,
            "subject_end": 1200,
        },
    ],
    "subject_len": 1200,
}

three_hsp_two_duplicates = {
    "element_len": 4949,
    "hsps": [
        {
            "query_end": 29246,
            "query_start": 24298,
            "subject_end": 4949,
            "subject_start": 1,
        },
        {
            "query_end": 29246,
            "query_start": 29209,
            "subject_end": 38,
            "subject_start": 1,
        },
        {
            "query_end": 24335,
            "query_start": 24298,
            "subject_end": 4949,
            "subject_start": 4912,
        },
    ],
    "subject_len": 4949,
}

"""
contig |--------------------------------------------------------|
mge 1     |------------|
mge 2                     |------------|
mge 3                                            |---|  |-----|
"""
four_hsps_three_mge = {
    "hsps": [
        {
            "query_start": 200,
            "query_end": 900,
            "subject_start": 100,
            "subject_end": 800,
        },
        {
            "query_start": 1100,
            "query_end": 1300,
            "subject_start": 1000,
            "subject_end": 1200,
        },
        {"query_start": 4000, "query_end": 4048, "subject_start": 1, "subject_end": 48},
        {
            "query_start": 4200,
            "query_end": 4248,
            "subject_start": 50,
            "subject_end": 1200,
        },
    ],
    "subject_len": 1200,
}

""" One MGE with multiple overlapping HSPs with repeted segments from subjects
HSPs location on sequence
=========================
MGE:   >--------------------------------------------------------------------------------<
hsp 1:                |-------|
hsp 2:                             |---|
hsp 3:                                                                       |---|
hsp 4:       |---|
hsp 5:                                                                       |---|
hsp 6:       |---|
hsp 7:         |-----|
hsp 8:                           |--|
hsp 9:                          |----|
hsp 10:                             ||

Origin of subject sequence
==========================
MGE:   >-------------------------------------------------------------------------------------------<
hsp 1:                                  |----|
hsp 2:        |--|
hsp 3:                                                                                     |--|
hsp 4:                                                                                     |-|
hsp 5:                  |-|
hsp 6:                  |-|
hsp 7:                          |---|
hsp 8:    |-|
hsp 9:                                       |--|
hsp 10:                                         ||
"""
one_mge_w_ten_hsps_w_overlapping_subject = {
    "hsps": [
        {
            "query_end": 18245,
            "query_start": 16244,
            "subject_end": 58041,
            "subject_start": 56050,
        },
        {
            "query_end": 21054,
            "query_start": 20203,
            "subject_end": 42214,
            "subject_start": 41363,
        },
        {
            "query_end": 33475,
            "query_start": 32649,
            "subject_end": 84717,
            "subject_start": 83891,
        },
        {
            "query_end": 14199,
            "query_start": 13379,
            "subject_end": 84718,
            "subject_start": 83898,
        },
        {
            "query_end": 33475,
            "query_start": 32656,
            "subject_end": 47855,
            "subject_start": 47036,
        },
        {
            "query_end": 14198,
            "query_start": 13379,
            "subject_end": 47855,
            "subject_start": 47036,
        },
        {
            "query_end": 15591,
            "query_start": 14199,
            "subject_end": 52751,
            "subject_start": 51357,
        },
        {
            "query_end": 20203,
            "query_start": 19590,
            "subject_end": 40054,
            "subject_start": 39441,
        },
        {
            "query_end": 20162,
            "query_start": 19046,
            "subject_end": 59647,
            "subject_start": 58541,
        },
        {
            "query_end": 20217,
            "query_start": 20181,
            "subject_end": 59724,
            "subject_start": 59688,
        },
    ],
    "subject_len": 86128,
}


def test_grouping_hsps():
    """Test grouping hsps."""
    # two hsps from same mge
    hit = mock_hit(**two_hsps_same_mge)
    r = _group_hsps(hit)
    assert len(r) == 1  # one group
    assert len(r[0]) == 2  # two entries in first group

    # three hsps of which two are duplicates
    # expected to keep largest
    hit = mock_hit(**three_hsp_two_duplicates)
    r = _group_hsps(hit)
    assert len(r) == 1
    assert len(r[0]) == 1  # one entries in first group

    # 4 hsps => (hsp 1, hsp2), (hsp3, hsp4). No duplicates
    hit = mock_hit(**four_hsps_three_mge)
    r = _group_hsps(hit)
    h = hit.hsps[2:4]
    h.reverse()
    exp = (tuple(hit.hsps[:2]), tuple(h))
    assert r == exp

    # one mge w ten hsps w overlapping subject
    hit = mock_hit(**one_mge_w_ten_hsps_w_overlapping_subject)
    r = _group_hsps(hit)
    assert len(r) == 1  # one group
    assert len(r[0]) == 5  # five entries in first group


def test_calc_posible_elem_coord():
    """Test calculation of possible element positions."""
    hsp = Mock(spec=BlastHsp)
    hsp.query_start = 600
    hsp.query_end = 800
    hsps = [hsp]
    assert (400, 1000) == _get_possible_elem_coord(hsps, 400)

    # Test start => 0
    hsp.query_start = 20
    hsp.query_end = 100
    hsps = [hsp]
    assert (0, 220) == _get_possible_elem_coord(hsps, 200)

    # test sanity check of elements
    with pytest.raises(CoordOutOfBoundsError):
        hsp.query_start = 100
        hsp.query_end = 600
        hsps = [hsp]
        assert (100, 500) == _get_possible_elem_coord(hsps, 200)


def test_sum_aln_cov():
    """Test summation of alignement coverage."""
    group = [
        {"query_start": 1, "query_end": 105, "num_gaps": 2},
        {"query_start": 200, "query_end": 402, "num_gaps": 5},
    ]
    mock_group = [mock_hsp(**h) for h in group]
    hit = Mock()
    hit.subject_len = 1200

    assert round(_calc_tot_aln_cov(mock_group, hit), 2) == 0.25


def test_get_padded_pos():
    """Get padded position."""
    hit = {
        "hsps": [
            {
                "num_gaps": 0,
                "query_end": 1000,
                "query_start": 100,
                "subject_end": 111,
                "subject_start": 1,
            }
        ],
        "element_len": 111,
        "subject_len": 123,
    }
    hit = mock_hit(**hit)

    # basic test
    res = _pad_mge_coord(hit, 0.1)
    exp = (55, 1045)
    assert res == exp

    # Test no negative coordinates
    res = _pad_mge_coord(hit, 2)
    exp = (0, 1900)
    assert res == exp


def test_bin_hits():
    """Test function for binning overlapping mges."""
    hits = [
        {
            "hsps": (
                {
                    "query_end": 197,
                    "query_start": 87,
                    "subject_end": 111,
                    "subject_start": 1,
                },
            ),
            "query_name": "NODE_1_length_361823_cov_8.17885_ID_6187",
            "subject_name": "MITEPlu5|1|BX470251",
        },
        {
            "hsps": (
                {
                    "query_end": 203,
                    "query_start": 90,
                    "subject_end": 117,
                    "subject_start": 1,
                },
            ),
            "query_name": "NODE_1_length_361823_cov_8.17885_ID_6187",
            "subject_name": "MITEEc1|1|U00096",
        },
        {
            "hsps": (
                {
                    "query_end": 309,
                    "query_start": 196,
                    "subject_end": 117,
                    "subject_start": 1,
                },
            ),
            "query_name": "NODE_1_length_361823_cov_8.17885_ID_6187",
            "subject_name": "MITEYpe1|1|CP009973",
        },
    ]
    m_hits = [mock_hit(**hit) for hit in hits]
    res = _bin_hits(iter(m_hits))
    # two bins, first with two entries, second with one
    exp = [m_hits[:2], m_hits[2:]]
    assert exp == res


def test_is_contained():
    """Test function for detecting nested mges."""
    # Success cases
    # =============

    # test first fully contained in second
    # 1. >----------------<
    # 2,     |xxxxxx|
    first = (200, 400)
    second = (100, 600)
    assert _is_contained(first, second)

    # Failure cases
    # =============

    # test second fully contained in first
    # 1.    >------<
    # 2,  |xxxxxxxxxxx|
    first = (100, 600)
    second = (200, 400)
    assert not _is_contained(first, second)

    # test first partly contained in second, overlap left
    # 1.      >----------------<
    # 2, |xxxxxx|
    first = (300, 900)
    second = (100, 400)
    assert not _is_contained(first, second)

    # test first partly contained in second, overlap right
    # 1. >----------------<
    # 2,              |xxxxxx|
    first = (300, 900)
    second = (800, 1200)
    assert not _is_contained(first, second)

    # test second not contained in first, shifted right
    # 1.             >----------------<
    # 2,  |xxxxxx|
    first = (100, 400)
    second = (800, 1200)
    assert not _is_contained(first, second)

    # test second not contained in first, shifted left
    # 1. >----------------<
    # 2,     |xxxxxx|
    first = (800, 1200)
    second = (100, 400)
    assert not _is_contained(first, second)


def test_join_cigar():
    """Test joining cigar strings."""
    hsp = [
        {"query_start": 1, "query_end": 10, "cigar": "A"},
        {"query_start": 15, "query_end": 20, "cigar": "B"},
        {"query_start": 22, "query_end": 30, "cigar": "C"},
    ]
    hsps = [mock_hsp(**h) for h in hsp]
    # test multiple hsps
    exp = "A N4 B N1 C"
    assert _join_cigar(hsps) == exp

    hsp = [
        {"query_start": 22, "query_end": 30, "cigar": "C"},
        {"query_start": 1, "query_end": 10, "cigar": "A"},
        {"query_start": 15, "query_end": 20, "cigar": "B"},
    ]
    # test multiple unordered hsps
    hsps = [mock_hsp(**h) for h in hsp]
    assert _join_cigar(hsps) == exp

    # test single hsps
    hsp = [{"query_start": 1, "query_end": 10, "cigar": "A"}]
    exp = "A"
    hsps = [mock_hsp(**h) for h in hsp]
    assert _join_cigar(hsps) == exp


def test_identify_mges(context_fixture, monkeypatch):
    """Test extraction of mge sequences."""
    # mock contig object
    mock_contig = MagicMock(spec=ContigSequences)

    # test file path
    blast_res = Path(__file__).parent.joinpath("data/single_is_blast_res.json")

    # disable functions for the function to work
    mock_attr = MagicMock(attr)
    monkeypatch.setattr("me_finder.predictor.detect.attr", mock_attr)
    raw_mges = _parse_raw_mge_alignments(blast_res)
    mges = _detect_mges(context_fixture, raw_mges, mock_contig)

    # assert contig seq is extracted correctly
    mock_contig.sub_sequence.assert_called_with(
        9194, 10903, 1, "NODE_54_length_17584_cov_7.73695_ID_6293"
    )

    # assert mge is predicted
    assert len(mges) == 3 and len(mges[0]) == 1
