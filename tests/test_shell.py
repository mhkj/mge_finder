"""Test shell invocation."""

import subprocess
from subprocess import CompletedProcess
from unittest.mock import Mock

import pytest
from me_finder.shell import ShellResult, _stringify_arg, run_shell


def test_run_shell(monkeypatch):
    """Test invocation of spades commands."""
    mock_subprocess = Mock(autospec=subprocess)
    mock_proc = Mock(spec=CompletedProcess)
    mock_proc.stdout = b"foo"
    mock_proc.stderr = b"bar"
    mock_proc.returncode = 1
    mock_subprocess.run.return_value = mock_proc

    monkeypatch.setattr("me_finder.shell.subprocess", mock_subprocess)

    command = ["some_command", "--flag", "4"]
    shell_res = run_shell(*command)
    mock_subprocess.run.assert_called_with(
        command, check=False, stdout=mock_subprocess.PIPE, stderr=mock_subprocess.PIPE
    )

    # Test that expected result object was created
    expected_result = ShellResult(
        cmd=" ".join(command), stdout="foo", stderr="bar", return_code=1
    )
    assert expected_result == shell_res


def test_stringify_args():
    """Test stringify args function."""
    # Expects pass
    assert _stringify_arg("a string") == "a string"

    assert _stringify_arg(r"a string") == "a string"

    assert _stringify_arg(r"a string") == "a string"

    assert _stringify_arg(True) == "True"

    assert _stringify_arg(12) == "12"

    assert _stringify_arg(12.12) == "12.12"

    # Exptects failure
    with pytest.raises(TypeError):
        _stringify_arg(list("a string"))

    with pytest.raises(TypeError):
        _stringify_arg(set("a string"))
