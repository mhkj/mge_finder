"""Test blast related functions."""

from unittest.mock import Mock

from me_finder.shell import ShellResult
from me_finder.tools import run_blast


def test_blast_invocation(monkeypatch, context_fixture):
    """Test invocation of blastn commands."""
    monkeypatch.setattr(
        "me_finder.tools.blast._get_executable_path", Mock(return_value="blastn")
    )
    mock_shell_result = Mock(spec=ShellResult)
    monkeypatch.setattr("me_finder.tools.blast.run_shell", mock_shell_result)

    context_fixture.num_threads = "2"
    context_fixture.dir_path.return_value = "blast"
    context_fixture.file_path.return_value = "blast_output.xml"
    context_fixture.blastn_path = "blastn"

    # Mock path isfile to disable file checks
    run_blast(context_fixture, "mge_records", "contigs.fasta")
    exp_result = [
        "blastn",
        "-query",
        "contigs.fasta",
        "-out",
        "blast_output.xml",
        "-db",
        "mge_records",
        "-num_threads",
        "2",
        "-word_size",
        11,
        "-outfmt",
        5,
    ]
    mock_shell_result.assert_called()
