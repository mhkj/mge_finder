#!/usr/bin/env python
"""Test cli interface."""

import logging
from configparser import ConfigParser
from unittest.mock import Mock

import pytest
from click.testing import CliRunner
from me_finder.cli import _get_fname, cli
from me_finder.tools import make_blast_database
from mgedb import MGEdb
from pkg_resources import resource_filename

FA_FILE_NAME = [
    ("DTU_2016_46_1_MG_CHE-67.bin.16.fa", "DTU_2016_46_1_MG_CHE-67.bin.16"),
    ("DTU_2016_46_1_MG_CHE-67.bin.16.fa.gz", "DTU_2016_46_1_MG_CHE-67.bin.16"),
    ("some/path/DTU_2016_46_1_MG_CHE-67.bin.16.fa", "DTU_2016_46_1_MG_CHE-67.bin.16"),
    (
        "some/path/DTU_2016_46_1_MG_CHE-67.bin.16.fa.gz",
        "DTU_2016_46_1_MG_CHE-67.bin.16",
    ),
    ("some/path/C00003590_n1_contigs.fasta", "C00003590_n1_contigs"),
    ("some/path/C00003590_n1_contigs.unknow_suffix", "C00003590_n1_contigs"),
]


@pytest.mark.parametrize("fa_fname,exp_sample_name", FA_FILE_NAME)
def test_assign_sample_name(fa_fname, exp_sample_name):
    """Test assignment of sample names from fasta file names."""
    assert _get_fname(fa_fname) == exp_sample_name


def test_find_no_params():
    """Test calling mge_finder find without subcommand."""
    runner = CliRunner()
    result = runner.invoke(cli, ["find"])
    assert result.exit_code == 2


def test_index_call(monkeypatch):
    """Test index cli command."""
    runner = CliRunner()
    # Mock mgedb instance
    mock_db = Mock(spec=MGEdb)
    mock_db.return_value = "db"
    monkeypatch.setattr("me_finder.cli.MGEdb", mock_db)
    # Mock make_blast_database function
    mock_index_db = Mock(make_blast_database)
    monkeypatch.setattr("me_finder.cli.make_blast_database", mock_index_db)

    runner.invoke(cli, ["index"])

    # test that function was called with db
    mock_index_db.assert_called_with("db", "makeblastdb")


def test_index_quiet_mode(monkeypatch):
    """Test setting of quiet mode index."""
    # Mock make_blast_database function
    mock_index_db = Mock(make_blast_database)
    monkeypatch.setattr("me_finder.cli.make_blast_database", mock_index_db)

    mock_logging = Mock(autospec=logging)
    logger = Mock(autospec=logging.Logger)
    mock_logging.getLogger.return_value = logger
    monkeypatch.setattr("me_finder.cli.logging", mock_logging)

    runner = CliRunner()
    result = runner.invoke(cli, ["--quiet", "index"])
    assert result.exit_code == 0

    result = runner.invoke(cli, ["-q", "index"])
    assert result.exit_code == 0

    # assert logger setup
    mock_logging.getLogger.assert_called()

    # assert ERROR level
    logger.setLevel.assert_called_with(mock_logging.ERROR)
    logger.setLevel.reset_mock()

    # assert INFO level
    runner.invoke(cli, ["index"])
    logger.setLevel.assert_not_called()


def test_read_default_config(monkeypatch):
    """Test reading of configuration file."""
    # mock configparser
    mock_cfg = Mock(autospec=ConfigParser)
    mock_parser = Mock(autospec=ConfigParser)
    mock_parser.return_value = mock_cfg
    # override config parser
    monkeypatch.setattr("me_finder.cli.ConfigParser", mock_parser)

    # patch and override pkg_resource filename
    mock_cfg_fname = Mock(autospec=resource_filename)
    exp_cfg_fname = "pkg_cfg.ini"
    mock_cfg_fname.return_value = exp_cfg_fname
    monkeypatch.setattr("me_finder.cli.resource_filename", mock_cfg_fname)

    runner = CliRunner()
    runner.invoke(cli, args=["find", "some_file"])

    # test that config from package was called
    # TODO fix test for reading config
    # mock_cfg.read.assert_called_with(exp_cfg_fname, encoding='utf-8')
