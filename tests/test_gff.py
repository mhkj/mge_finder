"""Test functions for converting to and writing gff format."""
from me_finder.tools.blast import _blast_aln_to_cigar


def test_aln_to_cirgar():
    """Test converting alignements to cigar string.

    Test cases are partly taken from GFF3 format specification.
    https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md
    """

    # Substitution in hit sequence
    qseq = "caagacctctggatatccaat"
    hseq = "caaCaccActggatatccaTt"
    expected = "M21"
    res = _blast_aln_to_cigar(qseq, hseq)
    assert res == expected

    # Three nt insertion
    qseq = "CAAGACCTCTGGAT---CAAT"
    hseq = "CAAGACCTCTGGATATCCAAT"
    expected = "M14 I3 M4"
    res = _blast_aln_to_cigar(qseq, hseq)
    assert res == expected

    # Three nt insertion
    qseq = "CA---CCTCTGGAT---CAAT"
    hseq = "CAAGACCTCTGGATATCCAAT"
    expected = "M2 I3 M9 I3 M4"
    res = _blast_aln_to_cigar(qseq, hseq)
    assert res == expected

    # Mix insertions and deletions
    qseq = "CAAGACCTAAACTGGAT-TCCAAT"
    hseq = "CAAGACCT---CTGGATATCCAAT"
    expected = "M8 D3 M6 I1 M6"
    res = _blast_aln_to_cigar(qseq, hseq)
    assert res == expected
