"""Test prediction of composite transposons."""

from glob import glob
from pathlib import Path
from unittest.mock import MagicMock, Mock

import attr
import pytest
from me_finder.io import ContigSequences
from me_finder.predictor.detect import _detect_mges
from me_finder.predictor.parse import _parse_raw_mge_alignments
from me_finder.predictor.putative import _predict_putative_composite_tn

# read file paths
TP_BLAST_RESULTS = glob(str(Path(__file__).parent.joinpath("data/comptn/tp_*.json")))
TN_BLAST_RESULTS = glob(str(Path(__file__).parent.joinpath("data/comptn/tn_*.json")))
# generate composite tn results, fmt: ([result, exp n comptn])
COMPTN_TEST_CASES = zip(
    TP_BLAST_RESULTS + TN_BLAST_RESULTS,
    [1] * len(TP_BLAST_RESULTS) + [0] * len(TN_BLAST_RESULTS),
)


@pytest.mark.parametrize("blast_result,expected_n_comptn", COMPTN_TEST_CASES)
def test_comptn_prediction(
    context_fixture, monkeypatch, blast_result, expected_n_comptn
):
    """Test prediction of putative comptn."""
    contig = ContigSequences(blast_result.replace("json", "fna"))
    blast_res = Path(__file__).parent.joinpath(blast_result)

    mock_attr = MagicMock(attr)
    monkeypatch.setattr("me_finder.predictor.detect.attr", mock_attr)

    # general mge detection
    raw_mges = _parse_raw_mge_alignments(blast_res)
    mges, _, _ = _detect_mges(context_fixture, raw_mges, contig)

    # assert detection of flanking is, sanity check
    assert len([mge for mge in mges if mge.type.value == "is"]) >= 1

    # predict putative comptn
    putative_comptn, _ = _predict_putative_composite_tn(
        context_fixture, mges, raw_mges, contig
    )
    assert len(putative_comptn) == expected_n_comptn  # check that comptn is detected
