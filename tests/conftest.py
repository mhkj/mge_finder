"""Shared functions for the tests suite."""
from configparser import ConfigParser
from unittest.mock import Mock

import pytest
from me_finder.cli import ExecutionContext
from mgedb import MGEdb


def config_fixture():
    """Context configuration."""
    parser = ConfigParser()
    test_config = {
        "spades": {
            "error_correction": True,
        },
        "blast": {
            "word_size": 11,
        },
        "validation": {
            "coverage": 0.9,
            "depth": 2,
            "e_value": -6,
            "identity": 0.8,
        },
        "putative_composite_transposon": {
            "max_len": 13000,
            "min_ir_segment_length": 60,
            "min_ir_aln_length": 20,
        },
    }
    parser.read_dict(test_config)
    return parser


def get_cnf(method, *args):
    """Get values from config fixture."""
    cnf = config_fixture()
    func = getattr(cnf, method)
    try:
        res = func(*args)
    except:
        res = True
    return res


@pytest.fixture()
def context_fixture():
    """Mock script execution context for tests."""
    ctx = Mock(spec=ExecutionContext)
    ctx.spades_path = "spades.py"
    ctx.config = config_fixture()
    ctx.cnf_get_default = get_cnf
    ctx.mge_db = MGEdb()
    return ctx
