#!/usr/bin/env python3

from io import StringIO
from unittest.mock import MagicMock, Mock

import pytest
from me_finder.io import ContigSequences, read_fasta_file
from mgedb import Sequence


RAW_FA_HEADERS = ["scaffold235 2.0", "scaffold235  2.0", "scaffold235     2.0"]
EXP_FA_HEADERS = ["scaffold235 2.0", "scaffold235 2.0", "scaffold235 2.0"]
FA_HEADERS = zip(RAW_FA_HEADERS, EXP_FA_HEADERS)


@pytest.mark.parametrize("raw_header_name,exp_header_name", FA_HEADERS)
def test_reading_contig_seqs(monkeypatch, raw_header_name, exp_header_name):
    """Test that contig headers are being sanetized to the same format as SimpleFastaParser."""
    CONTIG_SEQ = "GCAT"
    # mock reading fa files
    mock_fa_reader = Mock(
        read_fasta_file, return_value=[Sequence(title=raw_header_name, seq=CONTIG_SEQ)]
    )
    monkeypatch.setattr("me_finder.io.read_fasta_file", mock_fa_reader)
    # read contigs
    contigs = ContigSequences("some/path.fa")
    contigs._read_fa()
    assert exp_header_name == next(iter(contigs._fa.keys()))
