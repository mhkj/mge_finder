#!/usr/local/anaconda/bin/python2.7
################################################################################
##########                    CGE Service Wrapper                    ###########
################################################################################
# This script is part of the CGE Service structure
# --> The input/arguments are validated
# --> Create service folder and subfolders
# --> The uploaded files are copied to the 'Upload' directory
# --> Log service submission in SQL
# --> Setup and execution of service specific programs
# --> The success of the service is validated
# --> A HTML output page is created if in webmode
# --> Downloadable files are copied/moved to the 'Download' Directory
# --> The SQL service entry is updated
# --> The files are zipped for long-term storage

import json
import os
import random
import re
import sys
import time

from bs4 import BeautifulSoup

sys.path.append("/home/data1/services/CGEpipeline/CGEmodules")
from functions_module import (
    CheckFileType,
    FixPermissions,
    GraceFullExit,
    UpdatePaths,
    add2DatabaseLog,
    copyFile,
    createServiceDirs,
    fileUnzipper,
    fileZipper,
    getArguments,
    makeFileList,
    moveFile,
    paths,
    printDebug,
    printOut,
    program,
    progs,
    setDebug,
)

# INCLUDING THE CGE MODULES (No need to change this part)

################################################################################
##########                         FUNCTIONS                         ###########
################################################################################


def set_submit_button(dom, button_id, requested_file, service, version, run_id):
    """Set values for specified download buttons."""
    form = dom.select_one("#%s" % button_id)
    form.select_one("#inpt-service")["value"] = service
    form.select_one("#inpt-version")["value"] = version
    form.select_one("#inpt-filename")["value"] = requested_file
    form.select_one("#inpt-path-id")["value"] = run_id


def get_path(*args):
    """Get path and recursivly make directories."""
    path = os.path.join(*args)
    try:
        os.makedirs(path)
    except OSError:
        pass
    finally:
        return path


def add_prediction_result(result, template, service):
    """Add predction result to template html."""
    result_tag = template.select_one(service)  # data information
    var_dec = re.search(r"(let[\w\s]+)=", result_tag.string).group(1).strip()

    # set mgefinder result
    printDebug("add %s result as json string to output" % service)
    result_tag.string = "%s = `%s`" % (var_dec, json.dumps(result))


def parse_fasta(fh):
    """Parse fasta file."""
    header = None
    seq = ""
    for line in fh:
        if line.startswith(">"):
            if header is None:
                header = line[1:].strip()
            else:
                yield header, seq
                header = ""
                seq = ""
        else:
            seq += line.strip()
    yield header, seq


################################################################################
##########                           MAIN                            ###########
################################################################################
# SET GLOBAL VARIABLES

setDebug(False)
service, version = "MobileElementFinder", "1.0"
RESULT_TEMPLATE_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "result_template.html"
)
PLASMIDFINDER_PATH = "/home/data1/services/PlasmidFinder/PlasmidFinder-2.1/scripts/plasmidfinder/plasmidfinder.py"
RESFINDER_PATH = (
    "/home/data1/services/ResFinder/ResFinder-3.2/scripts/resfinder/resfinder.py"
)
VIRFINDER_PATH = "/home/data1/services/VirulenceFinder/VirulenceFinder-2.0/scripts/virulencefinder/virulencefinder.py"

# PARSE ARGUMENTS
# Add service specific arguments using the following format:
# (OPTION,   VARIABLE,  DEFAULT,  HELP)
# args = getArguments([
#   ('--uploadpath',  'uploadPath',  None, 'The folder containing uploads'),
#   ('-t',   'technology',  None, 'The sequencing platform of the input file')])
#
# Or by pasting the argument lines from the contigs file
args = getArguments(
    """
selectionbox   database    -d   VALUE
selectionbox   minlength   -l   VALUE
selectionbox   threshold   -t   VALUE
checkbox       resfind     -re  VALUE
checkbox       virfind     -vi  VALUE
""",
    allowcmd=True,
)

# VALIDATE REQUIRED ARGUMENTS
# if args.threshold == '': GraceFullExit("Error: No threshold was chosen!\n")
# if args.database == '' or args.database == None: GraceFullExit("Error: No database was chosen!\n")
# if args.technology is None: GraceFullExit("Error: No technology platform was chosen!\n")
if (
    args.uploadPath is None
    or len(args.uploadPath) < 5
    or not os.path.exists(args.uploadPath)
):
    GraceFullExit(
        "Error: No valid upload path was provided! (%s)\n" % (args.uploadPath)
    )
elif args.uploadPath[-1] != "/":
    args.uploadPath += "/"  # Add endslash

# SET RUN DIRECTORY (No need to change this part)
if args.reuseid:
    # /srv/www/secure-upload/isolates/5_16_4_2015_162_299_423438//0/
    run_id = [x for x in args.uploadPath.split("/") if x != ""][-2]
else:
    run_id = time.strftime("%w_%d_%m_%Y_%H%M%S_") + "%06d" % random.randint(0, 999999)
paths.serviceRoot = "{programRoot}IO/%s/" % (run_id)
paths.isolateRoot = "{programRoot}"
paths.add_path("uploads", "{serviceRoot}uploads/")

# SET AND UPDATE PATHS (No need to change this part)
paths.add_path("database", "/home/data1/services/{service}/database_repository/")
UpdatePaths(service, version, "", "", args.webmode)

# CREATE SERVICE DIRECTORIES (No need to change this part)
createServiceDirs()
stat = paths.Create("uploads")
args.technology = "fasta"
# LOG SERVICE SUBMISSION IN SQL (No need to change this part)
add2DatabaseLog(
    "%s-%s" % (service, version), run_id, args.usr, args.ip, args.technology
)

# MOVE UPLOADS FROM APP- TO ISOLATE UPLOAD DIRECTORY (No need to change this part)
if stat:
    # Move the uploaded files to the upload directory
    moveFile(args.uploadPath + "*", paths["uploads"])
    # Unzipping uploaded files if zipped
    fileUnzipper(paths["uploads"])
    # GET INPUT FILES from input path
    inputFiles = makeFileList(paths["uploads"])
else:
    GraceFullExit("Error: Could not create upload directory!\n")

# GET CONTIGS (No need to change this part UNLESS you dont need assembly)
# Only use fasta files
for infile in inputFiles:
    if CheckFileType(infile) == "fastq":
        GraceFullExit(
            "File is not an Assembled Genome / Fasta file (%s)!\nOnly fasta format is used in this service.\n"
            % (CheckFileType(infile))
        )

# Method = "blastn"
blastn_path = "/home/data1/tools/bin/Anaconda3-2.5.0/envs/py36_mge/bin/blastn"
makeblastdb_path = "/home/data1/tools/bin/Anaconda3-2.5.0/envs/py36_mge/bin/makeblastdb"
# Validate that only 1 file was submitted
if len(inputFiles) != 1:
    GraceFullExit("Error: Invalid number of contig files (%s)\n" % (len(inputFiles)))
# Validate that the uploaded file is fasta
if CheckFileType(inputFiles[0]) != "fasta":
    GraceFullExit(
        "Error: Invalid format (%s)!\nOnly the fasta format is recognised as a proper format.\n"
        % (CheckFileType(inputFiles[0]))
    )
# Add contigsPath

input_fname = os.path.basename(inputFiles[0])
paths.add_path("contigs", paths["inputs"] + input_fname)
# Copy file to Input directory
copyFile(inputFiles[0], paths["contigs"])

# Set database
database = os.path.join(paths["database"], "mgedb")

# Set specific environment-python
python_env = "/home/data1/tools/bin/Anaconda3-2.5.0/envs/py36_mge/bin/python3.6"

# ADDING PROGRAMS
mgefinder_output_base_path = os.path.join(paths["outputs"], "results")
mgefinder = progs.AddProgram(
    program(
        name="mge_finder",
        path=os.path.join(
            paths["scripts"], "MobileElementFinder/mobileElementFinder.py"
        ),
        ptype=python_env,
        toQueue=True,
        wait=False,
        workDir="",
        args=[
            "find",
            "--json",
            "--min-coverage",
            0.10,
            "--temp-dir",
            paths["outputs"],
            "--db-path",
            database,
            "--makeblastdb-path",
            makeblastdb_path,
            "--blastn-path",
            blastn_path,
            "--contig",
            paths["contigs"],
            mgefinder_output_base_path,
        ],
    )
)

# define mge finder outputs
mgefinder_wd_dir = os.path.join(paths["outputs"], input_fname.split(".")[0], "finder")
mge_seqs = os.path.join(paths["outputs"], "results_mge_sequences.fna")
printDebug("Mge sequence path: %s, %s" % (mge_seqs, os.path.isfile(mge_seqs)))
mge_result = os.path.join(mgefinder_wd_dir, "results.txt")
mge_json = "%s.json" % mgefinder_output_base_path
mge_csv = "%s.csv" % mgefinder_output_base_path

# Handle co-analysis with other CGE tools
GENE_LEN_THRES = 0.6
GENE_IDENT_THRES = 0.9

# PlasmidFinder
plsm_name = "plasmidfinder"
plsm_output = get_path(paths["outputs"], plsm_name)
plsm_dl = get_path(paths["downloads"], plsm_name)
plsm_finder = progs.AddProgram(
    program(
        name=plsm_name,
        path=PLASMIDFINDER_PATH,
        timer=0,
        ptype="python3",
        toQueue=True,
        wait=False,
        workDir=get_path(paths.serviceRoot, plsm_name),
        args=[
            "-i",
            paths["contigs"],  # contigs
            "-mp",
            "/home/data1/tools/bin/blastn",  # ADDED
            "-p",
            "/home/data1/services/PlasmidFinder/PlasmidFinder-2.1/database_2.1_02042019/",
            "-tmp",
            plsm_output,
            "-o",
            plsm_dl,
        ],
    )
)
plsm_result = os.path.join(paths["downloads"], plsm_name, "data.json")

# ResFinder
resf_name = "resfinder"
if args.resfind:
    resf_output = get_path(paths["outputs"], resf_name)
    resf_dl = get_path(paths["downloads"], resf_name)
    resfinder = progs.AddProgram(
        program(
            name=resf_name,
            path=RESFINDER_PATH,
            timer=0,
            ptype="python3",
            toQueue=True,
            wait=False,
            workDir=get_path(paths.serviceRoot, resf_name),
            args=[
                "-i",
                paths["contigs"],  # contigs
                "-mp",
                "/home/data1/tools/bin/blastn",  # ADDED
                "-p",
                "/home/data1/services/ResFinder/database_repository",
                "-t",
                GENE_IDENT_THRES,  # explicit default identity threshold
                "-l",
                GENE_LEN_THRES,  # explicit default length threshold
                "-tmp",
                resf_output,
                "-o",
                resf_dl,
                #'-x',  # extended output
                #'-d', args.anti,    # default to all databases
            ],
        )
    )

    progs.Add2List(resfinder)
resf_result = os.path.join(paths["outputs"], resf_name, "data_resfinder.json")

# VirulenceFinder
virf_name = "virulencefinder"
if args.virfind:
    virf_output = get_path(paths["outputs"], virf_name)
    virf_dl = get_path(paths["downloads"], virf_name)
    virulencefinder = progs.AddProgram(
        program(
            name=virf_name,
            path=VIRFINDER_PATH,
            timer=0,
            ptype="python3",
            toQueue=True,
            wait=False,
            workDir="",
            server="cgebase" if args.webmode else "cgebase2",
            args=[
                "-i",
                paths["contigs"],
                "-mp",
                "/home/data1/tools/bin/blastn",
                "-tmp",
                virf_output,
                "-o",
                virf_dl,
                "-p",
                "/home/data1/services/VirulenceFinder/database_repository/"
                #'-l', GENE_LEN_THRES,
                #'-t', GENE_IDENT_THRES,
                #'-x',   # extended output
            ],
        )
    )

    progs.Add2List(virulencefinder)
virf_result = os.path.join(paths["downloads"], virf_name, "data.json")

# EXECUTION OF THE PROGRAMS
mgefinder.Execute(True)
mgefinder.WaitOn(interval=10)
plsm_finder.Execute(True)
plsm_finder.WaitOn(interval=10)
# THE SUCCESS OF THE SERVICE IS VALIDATED
status = mgefinder.GetStatus()
if status != "Done":
    GraceFullExit("Error: Execution of the program MobileElementFinder failed!\n")

status = plsm_finder.GetStatus()
if status != "Done":
    GraceFullExit(
        "Error: Execution of the program MobileElementFinder, plasmid detection failed!\n"
    )

FixPermissions()

for progname in progs.list:
    progs[progname].Execute(True)

for progname in progs.list:
    if progs[progname].status == "Executing":
        progs[progname].WaitOn(interval=10)

for progname in progs.list:
    status = progs[progname].GetStatus()
    if status != "Done":
        GraceFullExit("Error: Execution of the program %s failed!\n" % progname)

FixPermissions()

# CREATE THE HTML OUTPUT FILE (No need to change this part)
mgefinder_res_output = os.path.join(paths["outputs"], "%s_debug.out" % service)
# if args.webmode:
#    if os.path.exists(paths['outputs']):
#        if not os.path.exists(mgefinder_res_output):
#            with open(mgefinder_res_output, 'w') as f:
#                pass

# move mge finder result files to dl folder
moveFile(mge_result, paths["downloads"])
moveFile(mge_csv, paths["downloads"])
copyFile(mge_seqs, paths["downloads"])
# moveFile(paths['outputs']+'results.gff',paths['downloads'])

# vir_out2html(os.path.join(paths['downloads'], 'results.txt'),
#             nuc_link=True,
#             twidth='90%',
#             css_classes="center virresults",
#             twoheaders=True)

# PREPARE THE DOWNLOADABLE FILE(S)
# mgefinder_dl_results = os.path.join(paths['downloads'], 'results.txt')
# mgefinder_dl_json = os.path.join(paths['downloads'], 'results.json')
# mgefinder_dl_seqs = os.path.join(paths['downloads'], 'mge_sequences.fna')
# mgefinder_dl_csv = os.path.join(paths['downloads'], 'results.csv')
# _ = PrepareDownload(mgefinder_dl_results)
# _ = PrepareDownload(mgefinder_dl_json)
# _ = PrepareDownload(mgefinder_dl_seqs)
# _ = PrepareDownload(mgefinder_dl_csv)
# _ = PrepareDownload(paths['downloads']+'Hit_in_genome_seq.fsa')
# _ = PrepareDownload(paths['downloads']+'results.gff')

# PRINT THE PROGRAM PARAMETERS TO THE HTML OUTPUT FILE
# if len(inputFiles) > 0:
#    printInputFilesHtml(inputFiles)

# PRINT THE CONTIGS INFORMATION TO THE HTML OUTPUT FILE
# if args.technology != 'Assembled_Genome': printContigsInfo(run_id=run_id)

################################################################################
####                     MGEfinder output generation                       #####
################################################################################

# The result is being generated by copying a template with the raw html layout.
# Result from the prediction tools are being read in and inserted as a json
# string in script tags.
# The json string are being converted on runtime and the information used
# by the script files to dynamically render the output.

# read output template
printDebug("read result template: %s" % RESULT_TEMPLATE_PATH)
with open(RESULT_TEMPLATE_PATH) as i:
    soup = BeautifulSoup(i, "lxml")

# store mgefinder results in memory
printDebug("read json result: %s" % mge_json)
with open(mge_json) as i:
    js = json.load(i)
    add_prediction_result(js, soup, "#mgefinder-prediction-result")

printDebug("read mge sequences result: %s" % mge_seqs)
with open(mge_seqs) as i:
    fa = {k: v for k, v in parse_fasta(i)}
    add_prediction_result(fa, soup, "#mgefinder-prediction-sequences")

# store plasmidfinder
with open(plsm_result) as i:
    js = json.load(i)
    add_prediction_result(js, soup, "#plasmidfinder-prediction-result")

if args.virfind:
    # store virulencefinder
    with open(virf_result) as i:
        js = json.load(i)
        add_prediction_result(js, soup, "#virulencefinder-prediction-result")

if args.resfind:
    # store resfinder
    with open(resf_result) as i:
        js = json.load(i)
        add_prediction_result(js, soup, "#resfinder-prediction-result")

# write result outfile for debug
printDebug("write debug mgefinder result html file: %s" % mgefinder_res_output)
with open(mgefinder_res_output, "w") as out:
    out.write(soup.select_one("#result-view-container").prettify())
printOut(soup.select_one("#result-view-container").prettify())

################################################################################
# LOG THE TIMERS (No need to change this part)
progs.PrintTimers()

# FIX THE PERMISSIONS OF THE SERVICE ROOT
FixPermissions()

# INDICATE THAT THE WRAPPER HAS FINISHED (No need to change this part)
printDebug("Done")

# GZIP ALL FILES IN THE SERVICE DIRECTORY AND SUBDIRS FOR LONG-TERM STORAGE (No need to change this part)
fileZipper(paths["serviceRoot"])
