.DEFAULT_GOAL:=help
SHELL:=/usr/bin/env sh

#Get tool version
VERSION = $(shell grep -oE "[0-9]+.[0-9]+.[0-9]+[a-z]*" me_finder/version.py)
TOOL_NAME = 'mobile_element_finder'
IMAGE_NAME_AND_TAG:=$(TOOL_NAME):$(VERSION)

DOCKER_RUN:=@docker run --rm -it					\
-v $(CURDIR)/tests:/usr/src/tests					\
-v $(CURDIR)/volumes/data:/usr/src/data				\
-v $(CURDIR)/volumes/finder:/tmp/mge_finder			\
$(IMAGE_NAME_AND_TAG)

help:  ## Show this message
	@awk '\
	BEGIN {FS = ":.*##"} \
	/^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } \
	/^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } \
	' $(MAKEFILE_LIST)

##@ Docker

.PHONY: build shell clean prune test type tag bump-version

build: ## Build docker image
	$(info -- Building docker image $(IMAGE_NAME_AND_TAG))
	docker image build --rm -t $(IMAGE_NAME_AND_TAG) .

bash: CMD=/bin/bash
bash: run CMD ## Spawn bash with image

run: ## Run a one-off command in a container. Specify using CMD (eg. make run CMD=bash)
	$(if $(CMD), $(DOCKER_RUN) $(CMD), $(error -- CMD must be set))

clean: ## Remove docker image
	@docker image rm $(IMAGE_NAME_AND_TAG)
	@rm -r MobileElementFinder.egg-info dist build

prune: ## Cleanup orphan docker resourses, results and data
	docker system prune --volumes -f
	rm -rf volumes/{finder,data}/*

test: CMD=pytest --cov=me_finder --ignore=me_finder/mgedb --cache-clear
test: run CMD

bump-version:  ## Tag the release
	@echo "Current version ${VERSION}"
	@read -p "Which should be increased (major.minor.patch): " target;		\
	if [[ $$target =~ (major|minor|patch)$$ ]]; then						\
	bump2version  $$target;													\
	else																	\
	echo $$target is not a valid option, use either major, minor or patch;	\
	fi
