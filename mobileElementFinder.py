#!/usr/bin/env python
"""Entry point for MobileElementFinder."""

from me_finder.cli import cli

if __name__ == "__main__":
    cli()
